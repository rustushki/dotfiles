#!/bin/bash

# OS Specific Exports
# ------------------------------------------------------------------------------
export EDITOR=vim
export fernflower=$(cygpath -w $CYGHOME/Source/fernflower/build/libs/fernflower.jar)
export JDK_5=
export JDK_6=
export JDK_7=$(cygpath -w $CYGHOME/Apps/openjdk-7.75)
export JDK_8=$(cygpath -w $CYGHOME/Apps/openjdk-8.402)
export JDK_9=
export JDK_10=
export JDK_11=
export JDK_12=
export JDK_13=
export JDK_14=
export JDK_15=
export JDK_16=
export JDK_17=$(cygpath -w $CYGHOME/Apps/openjdk-17.35)
export JDK_18=
export JDK_19=

# Pull in Non-OS Specific Exports, Aliases, and Functions
# ------------------------------------------------------------------------------
. ~/.bash_aliases_common

# OS Specific Aliases
# ------------------------------------------------------------------------------
# These are neat, but confusing if multiple vim instances are in use. Maybe explore more in the future
#alias v="vedit"
#alias vedit="nvr --servername=127.0.0.1:6790 --nostart --remote-tab"
#alias vl="vlog"
#alias vlog="nvr --servername=127.0.0.1:6789 --nostart --remote-tab"
alias v="vim"
alias vi="vim"
alias vimdiff="vim -d"
alias gsc="cd $UP/Source/dotfiles"
alias analyze_noreplay="java -jar '$(cygpath -w $CYGHOME/Source/developer/applications/ods/implementation/physical/noreplayinspector/target/noreplayanalyzer-1.0.0-SNAPSHOT.jar)'"
alias analyze_noreplay2="java -jar '$(cygpath -w $CYGHOME/Source/developer/applications/ods/implementation/physical/readveq/target/readveq-1.0.0-SNAPSHOT.jar)'"

alias cg_compile="mvn compile"

alias cg_build='mvn compile war:inplace -pl cytogen-web -am'

# Copies cytogenetics webapp from build folder to Tomcat
alias cg_deploy_no_exclude='rsync -a --delete cytogen-web/src/main/webapp/ $UP/Apps/tomcat/webapps/CytoWeb'

# Copies cytogenetics webapp from build folder to Tomcat; excludes certain folders which rarely change (cuts time copy time from 15s to 5s)
alias cg_deploy='rsync -a --delete --exclude jquery-ui-1.12.1.custom --exclude Cyto-Help --exclude assets --exclude ckeditor_sdk --exclude angular-1.7.2 --exclude bootstrap-table-1.23.0 --exclude bootstrap-4.1.1 --exclude bootstrap-5.3.3 --exclude fckeditor cytogen-web/src/main/webapp/ $UP/Apps/tomcat/webapps/CytoWeb'

 # - cg_grep (grep the cytogenetics folder with default filters for unwanted cgjava cruft)
alias cg_grep="maven_grep --exclude-dir='.git'"

alias cg_log="less /cygdrive/c/opt/IBM/WebSphere/AppServer/applogs/cytogenetics.log"

alias cg_debug="jdb -connect com.sun.jdi.SocketAttach:hostname=127.0.0.1,port=8000 -sourcepath $(cygpath -w $UP/Source/cytogenetics/cytogen-web/src/main/java/)"

# - tomcat_start (start tomcat) (DONE)
# optional: Add -Djavax.net.debug=all to log SSL network traffic
# optional: Add -Djdk.tls.client.protocols=TLSv1,TLSv1.1,TLSv1.2 to enable specific TLS protocols. Available in Java 1.7u95 or greater
alias tomcat_start="CATALINA_OPTS=\"-Djavax.net.ssl.trustStoreType=WINDOWS-ROOT -Xdebug -Xrunjdwp:transport=dt_socket,address=127.0.0.1:8000,server=y,suspend=n -Dcom.labcorp.app.env=local -Dcom.labcorp.app.appServerType=tomcat\" $(cygpath -u $CATALINA_HOME/bin/startup.bat)"
alias tomcat_start_jdk07="JAVA_HOME=\"$JDK_7\"  tomcat_start"
alias tomcat_start_jdk08="JAVA_HOME=\"$JDK_8\"  tomcat_start"
alias tomcat_start_jdk09="JAVA_HOME=\"$JDK_9\"  tomcat_start"
alias tomcat_start_jdk10="JAVA_HOME=\"$JDK_10\" tomcat_start"
alias tomcat_start_jdk11="JAVA_HOME=\"$JDK_11\" tomcat_start"
alias tomcat_start_jdk12="JAVA_HOME=\"$JDK_12\" tomcat_start"
alias tomcat_start_jdk13="JAVA_HOME=\"$JDK_13\" tomcat_start"
alias tomcat_start_jdk14="JAVA_HOME=\"$JDK_14\" tomcat_start"
alias tomcat_start_jdk15="JAVA_HOME=\"$JDK_15\" tomcat_start"
alias tomcat_start_jdk16="JAVA_HOME=\"$JDK_16\" tomcat_start"
alias tomcat_start_jdk17="JAVA_HOME=\"$JDK_17\" tomcat_start"
alias tomcat_start_jdk18="JAVA_HOME=\"$JDK_18\" tomcat_start"
alias tomcat_start_jdk19="JAVA_HOME=\"$JDK_19\" tomcat_start"
