#!/bin/bash

export UP=$(cygpath -u $USERPROFILE)
rm -rf $UP/AppData/Local/nvim
mkdir -p $UP/AppData/Local/nvim/lua/config
mkdir -p $UP/AppData/Local/nvim/lua/plugconfig
ln -f ./nvim/init.lua $UP/AppData/Local/nvim/init.lua
ln -f ./nvim/lua/plugins.lua $UP/AppData/Local/nvim/lua/plugins.lua
ln -f ./nvim/lua/set.lua $UP/AppData/Local/nvim/lua/set.lua
ln -f ./nvim/lua/mode.lua $UP/AppData/Local/nvim/lua/mode.lua
ln -f ./nvim/lua/config/keymaps.lua $UP/AppData/Local/nvim/lua/config/keymaps.lua
ln -f ./nvim/lua/auto.lua $UP/AppData/Local/nvim/lua/auto.lua
ln -f ./nvim/lua/functions.lua $UP/AppData/Local/nvim/lua/functions.lua
ln -f ./nvim/lua/plugconfig/gitsigns.lua $UP/AppData/Local/nvim/lua/plugconfig/gitsigns.lua
ln -f ./nvim/lua/plugconfig/litee.lua $UP/AppData/Local/nvim/lua/plugconfig/litee.lua
ln -f ./nvim/lua/plugconfig/nvim-lspconfig.lua $UP/AppData/Local/nvim/lua/plugconfig/nvim-lspconfig.lua
ln -f ./nvim/lua/plugconfig/nvim-cmp.lua $UP/AppData/Local/nvim/lua/plugconfig/nvim-cmp.lua
ln -f ./nvim/lua/plugconfig/telescope.lua $UP/AppData/Local/nvim/lua/plugconfig/telescope.lua
ln -f ./nvim/lua/plugconfig/vim-airline.lua $UP/AppData/Local/nvim/lua/plugconfig/vim-airline.lua
ln -f ./nvim/lua/plugconfig/vim-fswitch.lua $UP/AppData/Local/nvim/lua/plugconfig/vim-fswitch.lua
ln -f ./nvim/lua/plugconfig/vim-vimwiki.lua $UP/AppData/Local/nvim/lua/plugconfig/vim-vimwiki.lua

curl -s -k -fLo ./bash_windows/vim/colors/xoria256.vim --create-dirs https://raw.githubusercontent.com/vim-scripts/xoria256.vim/master/colors/xoria256.vim
rm -rf ~/.vimrc
rm -rf ~/.vim
mkdir -p ~/.vim/colors
ln -f ./git/gitosiconfig ~/.gitosiconfig
ln -f ./bash_windows/vim/colors/xoria256.vim ~/.vim/colors/xoria256.vim
ln -f ./bash_windows/vimrc ~/.vimrc
ln -f ./bash_windows/gitconfig ~/.gitconfig
ln -f ./bash_windows/bash_profile ~/.bash_profile
ln -f ./bash_windows/bash_aliases ~/.bash_aliases
ln -f ./bash_windows/inputrc ~/.inputrc
ln -f ./bash/bash_aliases_common ~/.bash_aliases_common

ln -f ./dbeaver/vrapperrc $UP/.vrapperrc
