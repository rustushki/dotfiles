-- Automatically read when a file has changed.  Do so on cursor movement - and
-- only if the current buffer has no unsaved changes.
vim.opt.autoread = true
vim.api.nvim_create_augroup("checktime", {})
if vim.fn.has("gui_running") == 0 then
	--silent! necessary otherwise throws errors when using command line window.
	vim.api.nvim_create_autocmd({"BufEnter", "CursorHold", "CursorHoldI", "CursorMoved", "CursorMovedI"}, {
		pattern = "*",
		command = "silent! checktime",
		group = "checktime"
	})
end

-- Automatically write on a variety of signifiers.  Primarily on ! or make.
vim.opt.autowrite = true

-- Automatically transfer the yank buffer to the win32 clipboard. This is much more efficient than always yanking to the
-- win32 clipboard.
if vim.fn.has("win32") == 1 then
	vim.api.nvim_create_augroup("copyonfocuslost", {})
	vim.api.nvim_create_autocmd("TextYankPost", {
		pattern = "*",
		command = "let g:copyToClipboard = 1",
		group = "copyonfocuslost"
	})

	vim.api.nvim_create_autocmd("FocusLost", {
		pattern = "*",
		command = "if g:copyToClipboard == 1 | let @+=@\" | let g:copyToClipboard = 0 | endif",
		group = "copyonfocuslost"
	})
end
