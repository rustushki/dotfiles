-- Default Tab Configuration
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.autoindent = true

-- Various Defaults
vim.opt.number = true                                         -- Add line numbers to left margin
vim.opt.modeline = true                                       -- Enable in-document modeline parsing
vim.opt.hlsearch = true                                       -- Highlight the search term when found in the document
vim.opt.wildmenu = true                                       -- Enable completion in Command-Line mode
vim.opt.hidden = true                                         -- Preserves undo tree even on buffer unloads.
vim.opt.termguicolors = true                                  -- Add better color to vim in terminal mode.
vim.opt.exrc = true                                           -- Enable per-project configurations
vim.opt.secure = true                                         -- Make per-project configurations more secure
vim.opt.wrap = false                                          -- Disable text wrapping
vim.opt.errorbells = false                                    -- Disable the beep or flash on error
vim.opt.mouse = 'a'                                           -- Enable mouse support in Normal, Visual, Insert, Command-Line and Help modes
vim.opt.laststatus = 2                                        -- Always display the status line (even if only 1 window)
vim.opt.backspace = '2'                                       -- Backspace works as you might expect in a text editor
vim.opt.textwidth = 120                                       -- It is 2019 after all
vim.opt.listchars='eol:$,tab:>-,trail:~,extends:>,precedes:<' -- Makes these whitespace chars visible. Use 'set list' to see them
vim.opt.list = false                                          -- Don't make whitespace chars visible by default
vim.g.loaded_perl_provider = 0                                -- Disable Perl Provider b/c CPAN Neovim::Ext 0.06 doesn't compile properly with nvim 0.10.1

if vim.fn.has("win32") == 1 then
    -- Set Cygwin to be terminal shell
    -- This is almost really neat. It mostly works, and fairly well at that.
    -- Unfortunately, escape sequences (including colors) are not always
    -- handled correctly. The result is that some operations don't work the way
    -- you'd expect. In particular, this creates problems with the cygwin-vim
    -- color scheme.
    --set vim.opt.shell = 'c:\\cygwin64\\bin\\bash --login -i'

	-- Windows environment only has python3 install as 'python'
	vim.g.python3_host_prog = '~/AppData/Local/Programs/Python/Python37/python.exe'

elseif vim.fn.has("unix") == 1 then
	-- RUSS20210920: Temporarily disabled in Windows this behavior
	-- Will monitor for performance improvements
	vim.opt.clipboard='unnamedplus,unnamed'       -- Yank to System Clipboard
end
