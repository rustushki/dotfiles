local nvim_lsp = require('lspconfig')
local util = require('lspconfig/util')

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
	local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end

	-- Mappings.
	local opts = { noremap=true, silent=true }

	-- See `:help vim.lsp.*` for documentation on any of the below functions
	buf_set_keymap('n', 'gD',        '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
	buf_set_keymap('n', 'gd',        '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
	buf_set_keymap('n', 'gi',        '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
	buf_set_keymap('n', 'gr',        '<cmd>lua vim.lsp.buf.references()<CR>', opts)
	buf_set_keymap('n', 'gci',       '<cmd>lua vim.lsp.buf.incoming_calls()<CR>', opts)
	buf_set_keymap('n', 'gco',       '<cmd>lua vim.lsp.buf.outgoing_calls()<CR>', opts)
	buf_set_keymap('n', 'gp',        '<cmd>lua vim.diagnostic.goto_prev{severity_limit = "Error"}<CR>', opts)
	buf_set_keymap('n', 'gn',        '<cmd>lua vim.diagnostic.goto_next{severity_limit = "Error"}<CR>', opts)
	buf_set_keymap('n', 'gP',        '<cmd>lua vim.diagnostic.goto_prev{}<CR>', opts)
	buf_set_keymap('n', 'gN',        '<cmd>lua vim.diagnostic.goto_next{}<CR>', opts)
	buf_set_keymap('n', 'K',         '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
	buf_set_keymap('n', '<C-k>',     '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
	buf_set_keymap('n', '<space>D',  '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
	buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
	buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
	buf_set_keymap('n', '<space>f',  '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
	buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
	buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
	buf_set_keymap('n', '<space>e',  '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
	buf_set_keymap('n', '<space>q',  '<cmd>lua vim.diagnostic.setloclist({severity = "error"})<CR>', opts)
	buf_set_keymap('n', '<space>Q',  '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
	buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
end


-- RUSS20241006: The usual behavior here is to set the default capabilities for the language server to be those of
-- cmp_nvim_lsp. However, starting in nvim 0.10, a new capability is supported in Windows called
-- 'didChangeWatchedFiles'. This functionality interferes with "completion" behavior in jdtls. By disabling the
-- copability, completion works normally again. NOTE: in Linux didChangeWatchedFiles is not supported in 0.10. The
-- presence of the bug in Windows vs the absence in Linux was the big clue.
local jdtls_capabilities = require('cmp_nvim_lsp').default_capabilities()
jdtls_capabilities.workspace = {}
jdtls_capabilities.workspace.didChangeWatchedFiles = {}
jdtls_capabilities.workspace.didChangeWatchedFiles.dynamicRegistration = false

nvim_lsp.jdtls.setup{
	on_attach = on_attach,

	-- RUSS20211031: Use '.git' has marker for root folder. Otherwise, it will sometimes use a submodule pom.xml and other times
	-- the root project pom.xml as the basis for the root dir. The result is that lspconfig will start multiple lsp
	-- clients and this leads to further unpredictable behavior.
	-- NOTE: This overrides the default root_dir function declared in lspconfig's jdtls.lua
	root_dir = function(fname)
		for _, patterns in ipairs({{'.git'}}) do
			local root = util.root_pattern(unpack(patterns))(fname)
			if root then
				return root
			end
		end
		return vim.fn.getcwd()
	end,

	-- RUSS20211031: Disable gradle. Project does not use gradle, but has some gradle files present
	settings = {
		java = {
			import = {
				gradle = {
					enabled = false
				}
			}
		}
	},

	capabilities = jdtls_capabilities,
}

nvim_lsp.clangd.setup{
	on_attach = on_attach,
	capabilities = require('cmp_nvim_lsp').default_capabilities()
}

-- This displays diagnostic errors as floating windows on hover rather than distracting inline text at the end of the
-- line which is often cut off by vertical splits
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
	vim.lsp.diagnostic.on_publish_diagnostics, {
		-- This will disable virtual text, like doing:
		-- let g:diagnostic_enable_virtual_text = 0
		virtual_text = false,

		-- This is similar to:
		-- let g:diagnostic_show_sign = 1
		-- To configure sign display,
		--  see: ":help vim.lsp.diagnostic.set_signs()"
		signs = true,

		-- This is similar to:
		-- "let g:diagnostic_insert_delay = 1"
		update_in_insert = false,
	}
)

local rt = require("rust-tools")
rt.setup({
	server = {
		on_attach = on_attach,
	},
})
