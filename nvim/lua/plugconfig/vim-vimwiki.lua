-- VimWiki Configuration
vim.g.vimwiki_list = {
	  {path = '~/Source/vimwiki_private_home'}
	, {path = '~/Source/vimwiki_private_work'}
	, {path = '~/Source/vimwiki_public'}
}

-- Turn off Markdown Table Autoformat in Windows. It's so very slow
if vim.fn.has 'win32' == 1 then
	vim.g.vimwiki_table_auto_fmt = 0
end
