if vim.fn.has("win32") == 1 then
    -- Disable Whitespace Checking in Vim Airline - Performance Boost for Large
    -- Files in Windows
    vim.g['airline#extensions#whitespace#enabled'] = 0
end
