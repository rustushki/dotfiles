-- Deletes all empty buffers which are those which are:
--  1. Listed as a user buffer
--  2. Have the empty name
--  3. Have no text contents
--  4. Have no changes
function _G.DeleteEmptyBuffers()
    local buffers = vim.fn.filter(vim.fn.range(1, vim.fn.bufnr('$')), 'buflisted(v:val) && empty(bufname(v:val)) && bufwinnr(v:val)<0 && !getbufvar(v:val, "&mod")')
    if next(buffers) ~= nil then
        vim.cmd('bdelete ' .. table.concat(buffers, ' '))
    end
end

function _G.JavaGenerateDebugStopAt()
	local currentLine = vim.api.nvim_win_get_cursor(0)[1]

	local className = ""
	for k in string.gmatch(vim.api.nvim_buf_get_name(0), ".*[/\\](.+).java$") do
		className = k
	end

	local package = ""
	for lineNumber, line in pairs(vim.api.nvim_buf_get_lines(0, 0, -1, true)) do
		for match in string.gmatch(line, "package%s*(.*);") do
			package = match
			break
		end

		if package ~= "" then
			break
		end
	end
	package = package.gsub(package, "%s", "")

	local stopAtCommand = "stop at " .. package .. "." .. className .. ":" .. currentLine

	vim.api.nvim_call_function("setreg", {"+", stopAtCommand})
	print("Copied: " .. stopAtCommand)
end

function _G.FormatFirefoxTablePaste1(delimiter)
	if delimiter == nil then
		delimiter = '§'
	end

	vim.cmd([[%s/\t/]] .. delimiter .. [[/g]])
	vim.cmd([[%s/ ]] .. delimiter .. [[/]] .. delimiter .. [[/g]])
end

function _G.FormatFirefoxTablePaste2RemoveLeadingRowNumber(delimiter)
	if delimiter == nil then
		delimiter = '§'
	end

	vim.cmd([[%s/^\s*\d\+\s*]] .. delimiter .. [[//]])
end

function _G.FormatFirefoxTablePaste3RemoveWrappingWhitespace(delimiter)
	if delimiter == nil then
		delimiter = '§'
	end

	vim.cmd([[%s/^\s*//g]])
	vim.cmd([[%s/\s*]] .. delimiter .. [[\s*/§/g]])
end
