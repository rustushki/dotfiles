local map = vim.keymap.set
local silent = {noremap = true, silent = true}

-- Force save the file in the current buffer.
--map('', '<F1>', '<C-C>:w! | sleep 100m | e<CR>', silent)
--map('i', '<F1>', '<C-C>:w! | sleep 100m | e<CR>', silent)
--map('', '<C-S>', '<C-C>:w! | sleep 100m | e<CR>', silent)
--map('i', '<C-S>', '<C-C>:w! | sleep 100m | e<CR>', silent)
map('', '<F1>', '<C-C>:w!<CR>', silent)
map('i', '<F1>', '<C-C>:w!<CR>', silent)
map('', '<C-S>', '<C-C>:w!<CR>', silent)
map('i', '<C-S>', '<C-C>:w!<CR>', silent)


-- Force quit the file in the current buffer. (discards changes)
map('', '<F4>', ':bp|bd! #<CR>', silent)
map('i', '<F4>', '<ESC>:bp|bd! #<CR>', silent)
map('', '<S-F4>', ':enew|bd! #<CR>', silent)
map('i', '<S-F4>', '<ESC>:enew|bd! #<CR>', silent)

-- Isolate the current buffer.
map('', '<F6>', ':only<CR>', silent)

-- Delete empty buffers and switch down to one tab (preserves all loaded files)
map('', '<F12>', '<ESC>:lua DeleteEmptyBuffers()<CR>', silent)

-- Clear Highlighted Search Results.
map('', '<Space>', ':noh<CR>', silent)

-- Replace the current window with an empty buffer.
map('', '<C-W>o', '<C-W>n<C-W><C-W><C-W>c', silent)

-- Forgot to edit as root?  Np.  Type w!!
if vim.fn.has("unix") == 1 then
	map('c', 'w!!', '%!sudo tee > /dev/null %', silent)
end

-- Leader Commands
map('', '<Leader>ey', ':let @*=@"<CR>', {silent = true, desc = 'Copy vim clipboard to system clipboard'})
map('', '<Leader>es', ':let @/=@"<CR>', {silent = true, desc = 'Copy vim clipboard to vim search clipboard'})
map('', '<Leader>ep', ':pwd<CR>', {silent = true, desc = 'Display the current working directory'})
map('', '<Leader>eo', ':on<CR>:tabo<CR>', {silent = true, desc = 'Make the close all tabs and windows except the current one'})
map('', '<Leader>ee', ':e!<CR>', {silent = true, desc = 'Reopen the current file from disk'})
map('', '<Leader>et2', ':set ai<CR>:set sta<CR>:set ts=2<CR>:set sts=2<CR>:set sw=2<CR>:set et<CR>', {silent = true, desc = 'Configure editor for 2 space tabs'})
map('', '<Leader>et4', ':set ai<CR>:set sta<CR>:set ts=4<CR>:set sts=4<CR>:set sw=4<CR>:set et<CR>', {silent = true, desc = 'Configure editor for 4 space tabs'})
map('', '<Leader>et8', ':set ai<CR>:set sta<CR>:set ts=8<CR>:set sts=8<CR>:set sw=8<CR>:set et<CR>', {silent = true, desc = 'Configure editor for 8 space tabs'})
map('', '<Leader>ett', ':set ai<CR>:set sta<CR>:set ts=4<CR>:set sts=4<CR>:set sw=4<CR>:set noet<CR>', {silent = true, desc = 'Configure editor for tabs'})

map('', '<Leader>ljd', ':lua JavaGenerateDebugStopAt()<CR>', {silent = true, desc = 'Create and Copy JDB Breakpoint Command'})
map('', '<Leader>lsf', ':%s/and/\\r  and/g | %s/,/\\r,/g | %s/where/\\rwhere/g | %s/from/\\rfrom/g<CR>', {silent = true, desc = "Format SQL (cheap)"})

map('', '<Leader>a', ':A<CR>', silent)
map('', '<Leader>cc', ':cwindow<CR>:cc<CR><c-w>bz<CR><CR>', silent)
map('', '<Leader>cn', ':cwindow<CR>:cn<CR><c-w>bz<CR><CR>', silent)
map('', '<Leader>cp', ':cwindow<CR>:cp<CR><c-w>bz<CR><CR>', silent)

-- Terminal Commands
map('t', '<F1>', '<C-\\><C-n>', silent)
map('t', '<A-h>', '<C-\\><C-n><C-w>h', silent)
map('t', '<A-j>', '<C-\\><C-n><C-w>j', silent)
map('t', '<A-k>', '<C-\\><C-n><C-w>k', silent)
map('t', '<A-l>', '<C-\\><C-n><C-w>l', silent)
map('i', '<A-h>', '<C-w>h', silent)
map('i', '<A-j>', '<C-w>j', silent)
map('i', '<A-k>', '<C-w>k', silent)
map('i', '<A-l>', '<C-w>l', silent)
map('n', '<A-h>', '<C-w>h', silent)
map('n', '<A-j>', '<C-w>j', silent)
map('n', '<A-k>', '<C-w>k', silent)
map('n', '<A-l>', '<C-w>l', silent)
map('t', '<C-PageUp>', '<C-\\><C-n><C-PageUp>', silent)
map('t', '<C-PageDown>', '<C-\\><C-n><C-PageDown>', silent)
map('t', '<C-p>', '<C-\\><C-n><C-p>', silent)
map('t', '<C-t>', '<C-\\><C-n><C-t>', silent)
map('t', '<F3>', ' <C-\\><C-n><F3>', silent)

-- Special: User presses C-H then a character and the contents of the character's register are pasted
map('t', '<C-H>', "'<C-\\><C-N>\"'.nr2char(getchar()).'pi'", { noremap = true, expr = true })

-- Windows Shift Insert to Paste
if vim.fn.has("win32") == 1 then
	map('', '<S-Insert>', '"+gP', silent)
	map('c', '<S-Insert>', '<C-R>+', silent)
	map('i', '<S-Insert>', '<ESC>:set paste<CR>a<C-R>+<ESC>:set nopaste<CR>a', silent)
	map('v', '<S-Insert>', '"+gP', silent)
end
