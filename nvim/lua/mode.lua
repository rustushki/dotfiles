vim.api.nvim_create_autocmd("FileChangedShell", {
	pattern = "*",
	callback = function()
		print("Warning: File changed on disk")
	end
})

vim.api.nvim_create_autocmd("TermOpen", {
	pattern = "*",
	callback = function()
		vim.wo.number = false
		vim.wo.list = false
		vim.wo.wrap = false
	end
})

if vim.opt.diff:get() then
	-- Set vimdiff theme.
	vim.cmd('colorscheme xoria256')
	vim.cmd('syntax off')
	vim.opt.background = 'dark'

	-- Skip the swap file warning if we're in diff mode.
	vim.opt.shortmess:append('A')

	-- Nice key combo to exit diffmode
	local default_opts = { noremap = true, silent = true }
	vim.api.nvim_set_keymap('n', '<bar><bar>', ':qa!<CR>', default_opts)

else
	vim.opt.background = 'dark'
	vim.cmd('colorscheme monokai-pro')
end
