return {
	{'nvim-telescope/telescope.nvim',
		init = function()
			require("plugconfig.telescope")
		end,
		keys = {
			{
				'<leader>tg',
				mode = {'n', 'x', 'c'},
				function()
					require("telescope.builtin").git_files()
				end,
				desc = 'Telescope Git Files'
			},
			{
				'<leader>tf',
				mode = {'n', 'x', 'c'},
				function()
					require("telescope.builtin").find_files()
				end,
				desc = 'Telescope Find Files'
			},
			{
				'<leader>tr',
				mode = {'n', 'x', 'c'},
				function()
					require("telescope.builtin").resume()
				end,
				desc = 'Telescope Resume - Open Last Picker'
			},
			{
				'<leader>th',
				mode = {'n', 'x', 'c'},
				function()
					require("telescope.builtin").help_tags()
				end,
				desc = 'Telescope Help Tags'
			},
			{
				'<leader>tb',
				mode = {'n', 'x', 'c'},
				function()
					require("telescope.builtin").buffers({sort_mru = true})
				end,
				desc = 'Telescope Buffers'
			},
			{
				'<leader>tm',
				mode = {'n', 'x', 'c'},
				function()
					require("telescope.builtin").keymaps()
				end,
				desc = 'Telescope Keymaps'
			}
		},
		dependencies = {
			"nvim-telescope/telescope-fzy-native.nvim",
			"nvim-lua/plenary.nvim"
		}
	},
	{'j-morano/buffer_manager.nvim',
		dependencies = {
			'nvim-lua/plenary.nvim'
		},
		keys = {
			{
				'<leader>eb',
				mode = {'n', 'x', 'c'},
				function()
					require("buffer_manager.ui").toggle_quick_menu()
				end,
				desc = 'Open buffer manager'
			}
		}
	},
	{'scrooloose/nerdtree',
		keys = {
			{
				'<F5>',
				'<cmd>:NERDTreeToggle<cr>',
				mode = {'n', 'i'},
				desc = 'NerdTree'
			}
		}
	},
	{'lewis6991/gitsigns.nvim',
		init = function()
			require("plugconfig.gitsigns")
			--require("gitsigns").setup()
		end,
		lazy = false
	},
	{'vimwiki/vimwiki',
		init = function()
			require("plugconfig.vim-vimwiki")
		end,
		lazy = false
	},
	{'vim-scripts/xoria256.vim'},
	{'folke/tokyonight.nvim'},
	{'nanotech/jellybeans.vim'},
	{'lucasprag/simpleblack'},
	{'loctvl842/monokai-pro.nvim', lazy = false},
	{'jaredgorski/SpaceCamp',
		config = function()
			vim.cmd([[colorscheme spacecamp]])
		end,
		lazy = false
	},
	{'vim-airline/vim-airline',
		dependencies = {
			'vim-airline/vim-airline-themes'
		},
		init = function()
			require("plugconfig.vim-airline")
		end,
		lazy = false
	},
	{'hrsh7th/vim-vsnip',
		lazy = false
	},
	{'hrsh7th/cmp-nvim-lsp',
		dependencies = {
			'neovim/nvim-lspconfig'
		}
	},
	{'hrsh7th/cmp-buffer'},
	{'hrsh7th/cmp-path'},
	{'hrsh7th/cmp-cmdline'},
	{'hrsh7th/nvim-cmp',
		dependencies = {
			'hrsh7th/cmp-nvim-lsp',
			'hrsh7th/cmp-buffer',
			'hrsh7th/cmp-path',
			'hrsh7th/cmp-cmdline',
			'hrsh7th/vim-vsnip'
		},
		init = function()
			require("plugconfig.nvim-cmp")
		end
	},
	{'derekwyatt/vim-fswitch',
		ft = {'cpp', 'hpp', 'c', 'h', 'cxx'},
		lazy = true,
		init = function()
			require("plugconfig.vim-fswitch")
		end
	},
	{'nvim-treesitter/nvim-treesitter'
		, cmd = 'TSUpdate'
		, enabled = jit.os:find("Linux") ~= nil
	},
	{'neovim/nvim-lspconfig',
		init = function()
			require("plugconfig.nvim-lspconfig")
		end
	},
	{'ldelossa/litee.nvim',
		dependencies = {'neovim/nvim-lspconfig'},
		init = function()
			require("plugconfig.litee")
		end
	},
	{'ldelossa/litee-calltree.nvim',
		dependencies = {'ldelossa/litee.nvim'},
		keys = {
			{
				'<leader>eci',
				mode = {'n', 'x', 'c'},
				function()
					vim.lsp.buf.incoming_calls()
				end,
				desc = 'LSP Calltree Incoming Calls'
			},
			{
				'<leader>eco',
				mode = {'n', 'x', 'c'},
				function()
					vim.lsp.buf.outgoing_calls()
				end,
				desc = 'LSP Calltree Outgoing Calls'
			},
		}
	},
	{
		"echasnovski/mini.icons"
	},
	{
		"folke/trouble.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		opts = {
			severity = vim.diagnostic.severity.ERROR
		},
		keys = {
			{
				'<leader>edo',
				mode = {'n', 'x', 'c'},
				function()
					require("trouble").open()
				end,
				desc = 'Open Diagnostics (impl: Trouble)'
			},
			{
				'<leader>edc',
				mode = {'n', 'x', 'c'},
				function()
					require("trouble").close()
				end,
				desc = 'Close Diagnostics (impl: Trouble)'
			},
		}
	},
	{"folke/which-key.nvim",
		event = "VeryLazy",
		opts = {
			plugins = { spelling = true },
		},
		config = function(_, opts)
			local wk = require("which-key")
			wk.setup(opts)
			wk.add({
				{"<leader>e", group = "+editor"},
				{"<leader>ec", group = "+calltree"},
				{"<leader>et", group = "+identation settings"},
				{"<leader>t", group = "+telescope"},
				{"<leader>w", group = "+vimwiki"},
				{"<leader>l", group = "+language specific"},
				{"<leader>lj", group = "+java specific"},
				{"<leader>ls", group = "+sql specific"},
			})
		end,
	},
	{
		"williamboman/mason.nvim",
		init = function()
			require("mason").setup()
		end
	},
	{
		"williamboman/mason-lspconfig.nvim"
	},
	{
		"simrat39/rust-tools.nvim"
	}
}
