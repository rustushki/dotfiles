#!/bin/bash

ln -sfT $PWD/nvim ~/.config/nvim

ln -sfT $PWD/git/gitconfig ~/.gitconfig
ln -sfT $PWD/git/gitosiconfig ~/.gitosiconfig

ln -sfT $PWD/bash/bash_aliases_common ~/.bash_aliases_common
ln -sfT $PWD/bash/bash_aliases ~/.bash_aliases
ln -sfT $PWD/bash/bashrc ~/.bashrc

sudo apt install powerline python3-powerline python3-powerline-gitstatus
wget -qO - https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/CascadiaMono.zip | busybox unzip - -d ~/.local/share/fonts/

ln -sfT $PWD/kitty/kitty.conf ~/.config/kitty/kitty.conf
